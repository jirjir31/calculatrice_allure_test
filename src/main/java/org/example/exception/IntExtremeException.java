package org.example.exception;

import java.io.IOException;

public class IntExtremeException extends RuntimeException {

    public IntExtremeException(){
        super("chiffres trop petits ou trop grands");
    }
}
