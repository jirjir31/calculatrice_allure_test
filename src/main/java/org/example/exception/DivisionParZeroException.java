package org.example.exception;

public class DivisionParZeroException extends RuntimeException {

    public DivisionParZeroException(){
        super("Division par zero interdite");
    }
}
