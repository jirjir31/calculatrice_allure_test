package org.example;

import org.example.exception.DivisionParZeroException;
import org.example.exception.IntExtremeException;

public class Calculatrice {

    public int additionner(int param1, int param2) throws IntExtremeException {
        //这里必须要转换成long，否则int无法承受这么大/小的数据，它的值会诡异变化
        //        System.out.println(Integer.MAX_VALUE); //2147483647
        //        System.out.println(Integer.MAX_VALUE + 3); //-2147483646
        //        System.out.println((long)Integer.MAX_VALUE+3); //2147483650
        if((long)param1+param2 > Integer.MAX_VALUE || (long)param1+param2 < Integer.MIN_VALUE){
            throw new IntExtremeException();
        }
        return param1 + param2;
    }

    public double divier(int param1, int param2) throws DivisionParZeroException {
        if(param2 == 0 ){
            throw new DivisionParZeroException();
        }else{
            return (double)param1/param2;
        }
    }


    public int soustraire(int param1, int param2) throws IntExtremeException{
        if((long)param1-param2 > Integer.MAX_VALUE || (long)param1-param2 < Integer.MIN_VALUE){
            throw new IntExtremeException();
        }else{
            return param1-param2;
        }
    }


    public int multiplier(int param1, int param2) throws IntExtremeException {
        if ((long) param1 * param2 > Integer.MAX_VALUE || (long) param1 * param2 < Integer.MIN_VALUE) {
            throw new IntExtremeException();
        } else {
            return param1 * param2;
        }
    }


}
