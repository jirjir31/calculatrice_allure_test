package org.example;

import org.example.package1.AdditionnerTest;
import org.example.package1.SoustraireTest;
import org.example.package2.DiviserTest;
import org.example.package2.MultiplierTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages("org.exemple")
@SelectClasses({AdditionnerTest.class, SoustraireTest.class, DiviserTest.class, MultiplierTest.class})

public class AllMyTests {
}
