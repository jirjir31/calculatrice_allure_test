package org.example;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.ExcludeTags;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;


//AdditionnerTest: prod + dev
//le rest : prod
@RunWith(JUnitPlatform.class)
@SelectPackages("org.example")
@IncludeTags("prod")
//sans  ExcludeTags("dev") => AdditionnerTest sera executé
//avec  ExcludeTags("dev") => AdditionnerTest ne sera pas executé
@ExcludeTags("dev")
public class ProdTest {

}
