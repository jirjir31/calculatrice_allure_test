package org.example.package2;

import org.example.Calculatrice;
import org.example.exception.DivisionParZeroException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("prod")
public class DiviserTest {

    Calculatrice cal = new Calculatrice();

    @Test
    void divisionTest(){

        Assertions.assertEquals(3, cal.divier(12,4), "12/4=3");
    }

   // @Disabled //valable pour methode et classe
    @Test
    @Tag("exception")
    void divisionParZeroTest(){

        Assertions.assertThrows(DivisionParZeroException.class, ()->cal.divier(2,0));
    }

}
