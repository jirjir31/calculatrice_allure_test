package org.example.package2;

import org.example.Calculatrice;
import org.example.exception.IntExtremeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;

@Tag("prod")
public class MultiplierTest {

    Calculatrice cal = new Calculatrice();

    @Test
    void testMultiplication(){
        Assertions.assertEquals(18, cal.multiplier(3,6), "3*6=18");
        Assertions.assertTrue(-18 == cal.multiplier(-3, 6), "-3*6=-18");
        Assertions.assertTrue(0 == cal.multiplier(0, 6), "0*6=0");

    }

    @Test
    @Tag("exception")
    void testMultiplicationException(){
        Assertions.assertThrows(IntExtremeException.class, ()->cal.multiplier(Integer.MAX_VALUE, 2));
        Assertions.assertThrows(IntExtremeException.class, ()->cal.multiplier(Integer.MIN_VALUE, 2));
    }


}
