//package org.example;
//
//import org.junit.platform.launcher.LauncherDiscoveryRequest;
//import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
//import org.junit.platform.launcher.core.LauncherFactory;
//
//import static org.junit.platform.engine.discovery.ClassNameFilter.includeClassNamePatterns;
//import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
//import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;
//
//public class Launcher {
//
//    public static void main( String[] args )
//    {
//
//        //设置搜索和过滤规则
//        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
//                .selectors(
//                        selectPackage("org.example"),
//                        //selectClass("DevTests.class")
////                        selectClass(AdditionnerTest.class),
////                        selectClass(SoustraireTest.class),
//                        selectClass(AllMyTests.class)
//                )
//                .filters(
//                        //includeClassNamePatterns(".*Tests")
//                        includeClassNamePatterns("^(Test.*|.+[.$]Test.*|.*Tests?)$")
//
//                )
//                .build();
//
//        System.out.println(request);
//
//
//
//        org.junit.platform.launcher.Launcher launcher = LauncherFactory.create();
//        // Register a listener of your choice
//        //通过监听器来监听获取执行结果
////        TestExecutionListener listener = new SummaryGeneratingListener();
////        launcher.registerTestExecutionListeners(listener);
//        launcher.execute(request);
//
//        System.out.println("tests executes");
//    }
//
//}
