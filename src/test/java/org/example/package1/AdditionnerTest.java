package org.example.package1;

import org.example.Calculatrice;
import org.example.exception.IntExtremeException;
import org.junit.jupiter.api.*;

@Tag("dev")
@Tag("prod")//Tag possible pour classe et methode
public class AdditionnerTest {


    Calculatrice cal = new Calculatrice();

    @Test
    @DisplayName("test pour methode additionner")
    //@RepeatedTest(2) //mais execute au total 1+2=3 fois
    @RepeatedTest(value = 2, name = "{displayName}, repetition {currentRepetition} sur {totalRepetitions}")
    void TestAddition() throws IntExtremeException {
        System.out.println("additionTest");

        Assertions.assertEquals(7, cal.additionner(3,4), "3+4=7");
        Assertions.assertEquals(-7, cal.additionner(-5,-2), "-5-2=-7");
        Assertions.assertEquals(-6, cal.additionner(3,-9), "3-9=-6");
    }

    @Test
    @Tag("exception")
    void TestAdditionException1(){
        Assertions.assertThrows(IntExtremeException.class, ()->cal.additionner(Integer.MAX_VALUE, 3));
        Assertions.assertThrows(IntExtremeException.class, ()->cal.additionner(Integer.MIN_VALUE, -2));
    }

    //test pour générer des test failure
    @Test
    //@Disabled
    void TestAdditionFail() throws IntExtremeException {
        System.out.println("additionTest");

        Assertions.assertEquals(17, cal.additionner(3,4), "3+4=7");
    }

    @BeforeAll
    static void init(){
        System.out.println("BeforeAll Annotation test");
    }

    @AfterAll
    static void clearUpAll(){
        System.out.println("AfterAll Annotation test");
    }

    @BeforeEach
    void eachInit(){
        System.out.println("BeforeEach Annotation test");
    }

    @AfterEach
    void clearUp(){
        System.out.println("AfterEach Annotation test");
    }

}
