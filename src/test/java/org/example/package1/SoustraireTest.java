package org.example.package1;

import org.example.Calculatrice;
import org.example.exception.IntExtremeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("prod")
public class SoustraireTest {

    Calculatrice cal = new Calculatrice();

    @Test
    void testSoustraction() {
        Assertions.assertEquals(3, cal.soustraire(9,6), "9-6=3");
        Assertions.assertEquals(-1, cal.soustraire(-2,-1), "-2-(-1)=-1");
    }

    @Test
    @Tag("exception")
    void testSoustractionException(){
        Assertions.assertThrows(IntExtremeException.class, ()->cal.soustraire(Integer.MIN_VALUE, 2));
        Assertions.assertThrows(IntExtremeException.class, ()->cal.soustraire(Integer.MAX_VALUE, -2));
    }
}