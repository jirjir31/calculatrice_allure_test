import org.example.AllMyTests;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import java.io.PrintWriter;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;


//public class App {
//    public static void main( String[] args ) {
//
//        //设置搜索和过滤规则
//        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
//                .selectors(
//                        selectPackage("org.example"),
//                        selectClass(AllMyTests.class)
//                )
//                .filters(
//                        //includeClassNamePatterns(".*Tests")
//                        includeClassNamePatterns("^(Test.*|.+[.$]Test.*|.*Tests?)$")
//
//                )
//                .build();
//
//        System.out.println(request);
//
//
//
//        org.junit.platform.launcher.Launcher launcher = LauncherFactory.create();
//        TestPlan plan = launcher.discover(request);
//
//        TestExecutionListener listener = new SummaryGeneratingListener();
//        launcher.registerTestExecutionListeners(listener);
//
//        launcher.execute(request, listener);
//
//        System.out.println("tests executes");
//    }
//
//
//}


public class App {
    public static void main(String[] args) {

        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(
                        selectPackage("org.example")
                )
                .build();

        Launcher launcher = LauncherFactory.create();

        SummaryGeneratingListener listener = new SummaryGeneratingListener();
        launcher.registerTestExecutionListeners(listener);

        launcher.execute(request);

        TestExecutionSummary summary = listener.getSummary();
        summary.printFailuresTo(new PrintWriter(System.out));
        summary.printTo(new PrintWriter(System.out));

    }
}
